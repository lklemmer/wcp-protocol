import socket
import json


class WcpConnection:
    version = "0"

    def __init__(self, addr="127.0.0.1", port=54321, verbose=False):
        self.addr = addr
        self.port = port
        self.verbose = verbose
        self.buffer = b""
        self.server_supported_commands = []
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.addr, self.port))
        response = self.send(
            {
                "type": "greeting",
                "version": WcpConnection.version,
                "commands": [
                    "add_variables",
                    "get_item_list",
                    "get_item_info",
                    "set_item_color",
                ],
            }
        )

        if response["type"] == "error":
            raise Exception(response["message"])
        elif response["version"] == WcpConnection.version:
            self.server_supported_commands = response["commands"]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.socket.close()

    def log(self, *args):
        if self.verbose:
            print(*args)

    def send(self, message: str):
        message = json.dumps(message).encode()
        self.log(f"C>S: {message!r}")
        self.socket.send(message + b"\0")

        # read response
        data = self.read_message()
        self.log(f"S>C: {data!r}")
        return data

    def read_message(self):
        raw_data = self.buffer
        self.buffer = b""
        acc = raw_data
        while b"\x00" not in raw_data:
            raw_data = self.socket.recv(1024)
            if not raw_data:
                return None

            acc += raw_data

        data = str(acc, "utf-8").split("\x00")
        if len(data) > 1 and data[1]:
            self.buffer = bytes("\x00".join(data[1:]), "ascii")

        return json.loads(data[0])

    def add_variables(self, variables: [str]):
        assert all(
            isinstance(var, str) for var in variables
        ), "Arguments must be strings"
        response = self.send(
            {"type": "command", "command": "add_variables", "variables": variables}
        )

        if response["type"] == "response":
            return response["ids"]
        else:
            raise Exception(response["message"])

    def add_scope(self, scope: str):
        assert isinstance(scope, str), "Scope name must be a string"
        response = self.send({"type": "command", "command": "add_scope", "scope": scope})

        if response["type"] == "response":
            return response["ids"]
        else:
            raise Exception(response["message"])

    def remove_items(self, ids: [int | str]):
        assert all(
            isinstance(id, (int, str)) for id in ids
        ), "ids must be integers or strings"
        response = self.send(
            {"type": "command", "command": "remove_items", "ids": ids}
        )

        if response["type"] == "response":
            return True
        else:
            raise Exception(response["message"])

    def focus_item(self, id: [int | str]):
        assert isinstance(id, (int, str)), "id must be an int or a string"
        response = self.send(
            {"type": "command", "command": "focus_item", "id": id}
        )

        if response["type"] == "response":
            return True
        else:
            raise Exception(response["message"])

    def get_item_list(self):
        response = self.send(
            {
                "type": "command",
                "command": "get_item_list",
            }
        )

        if response["type"] == "response":
            return response["ids"]
        else:
            print("Error:", response)

    def get_item_info(self, item_ids: [int | str]):
        assert all(
            isinstance(item_id, (int, str)) for item_id in item_ids
        ), "Arguments must be strings or integers"
        response = self.send(
            {"type": "command", "command": "get_item_info", "ids": item_ids}
        )

        if response["type"] == "response":
            return response["results"]
        else:
            raise Exception(response["message"])

    def set_item_color(self, item_id: int | str, color_name: str):
        assert isinstance(item_id, (int, str))
        assert isinstance(color_name, str)

        self.send(
            {
                "type": "command",
                "command": "set_item_color",
                "id": item_id,
                "color": color_name,
            }
        )

    def set_viewport_to(self, timestamp: int):
        assert isinstance(timestamp, int), "timestamp must be an int"
        response = self.send(
            {"type": "command", "command": "set_viewport_to", "timestamp": timestamp}
        )

        if response["type"] == "response":
            return True
        else:
            raise Exception(response["message"])
