# Waveform Viewer Control Protocol

## WCP basics

The WCP allows remote control of waveform viewers (_servers_) from other
programs (_clients_). The server and client communicate via JSON messages.

WCP supports commands initiated by the client where the client
sends a _command_ to which the server responds with a _response_ or _error_.
Commands can be pipelined, i.e. the client can send multiple commands before
receiving replies. The server will reply in the order that the commands were
sent.

For asynchronous "events", the server can also send _event_ messages to the client.

## Message transport

Communication between the client and server is done using JSON messages. If the communication
is done using a message transport system that supports structured packets, each message
is sent as an individual packet containing a string with the JSON message.

When communication between the client and server is done using byte streams
such as TCP or stdio, each message is sent as UTF-8 encoded JSON and separated
by a null byte (`U+0000`).


## Common Types and Concepts

The WCP is designed around traditional waveform viewers where variables,
dividers, timelines and other "items" are displayed in a vertical list with the
corresponding values in an adjacent viewport.

```
    Item List
   +----------------+--------------------------------------+
   |Name        (ID)|                                      |
L I|Variable    (1) |___/----\________/----\________/----\_|
i t|Variable    (2) |---{0xff}-----------------------------|
s e|Divider     (3) |                                      |
t m|      .         |                                      |
e s|      .         |                                      |
d  |      .         |                                      |
   |Listed Item (n) |                                      |
   +-------------------------------------------------------+
```

### `DisplayedItemRef`

All displayed items in the waveform viewer have a unique `DisplayedItemRef`
which is used to refer to that specific item. The format of this value is
defined by the server and should be treated as an opaque value by a client,
i.e. a client should never create a new `DisplayedItemRef`. It can only use
values previously received from the server, for example when adding signals.

The `DisplayedItemRef` can be a string or an integer.

### `ItemInfo`

The `ItemInfo` structure contains information about items.

`{
  name: string,
  type: string,
  id: DisplayedItemRef
}`

### `VariablePath`

A string representing the hierarchical path to a variable. Modules are separated by `.`

Example:

`top.submodule.variable`

### `Scope`

A string representing the hierarchical path to a scope. Modules are separated by `.`

Example:

`top.submodule`

## Greeting

When a client first connects to a server, it sends a `greeting` message which
contains the protocol version it supports, along with the supported features.

```typescript
{
    "type": "greeting",
    "version": string,
    "commands": [string]
}
```


If the server supports the specified protocol version, it replies with a
similar greeting, along with the features it supports.

```typescript
{
    "type": "greeting",
    "version": string,
    "commands": [string]
}
```

If the server does not support the specified protocol version, it replies with a `Error` message.

## Message type: `Command`

In order to control the waveform viewer, the client sends a command message:
```
C>S:

{
    "type": "command",
    "command": "<command name>",
    "<arg1 name>": "<arg1 value>"
    "<arg2 name>": "<arg2 value>"
    ...
    "<argN name>": "<argN value>"
}
```

For each command received, the server replies with either a `Response` message if the request
was successful, or a `Error` message if it was unsuccessful.


The following commands are supported

### Command: `get_item_list`

Responds with a `get_item_list` response which contains a list of the currently displayed
items

### Command: `get_item_info`

Returns `ItemInfo` for each item in the `ids` list. The order of the returned info
is the same as the IDs. If any of the items in `ids` is missing, an error is returned.

```typescript
{
  type: "command",
  command: "get_item_info",
  ids: [DisplayedItemRef],
}
```

### Command: `set_item_color`

Changes the color of the item with the specified id. `color` is the name of
the color to set. Which colors are supported is defined by the server. If the
color is invalid, nothing happens. If the `ID` does not exist, an error is
sent. If no error occurred, an `ack` response is sent.

```typescript
{
  type: "command",
  command: "set_item_color",
  id: DisplayedItemRef,
  color: string,
}
```


### Command: `add_variables`

Adds variables with the specified paths to currently displayed variables.
Responds with a `add_variables` response which contains the `DisplayedItemRef`s for each
added variable in order. If any of the variables was not found, an error is returned instead.

```typescript
{
  type: "command",
  command: "add_variables",
  variables: [VariablePath]
}
```

### Command: `add_scope`

Adds all variables in the specified scope to the currently displayed variables.
Responds with a `add_scope` response which contains the `DisplayedItemRef`s for each
added variable. If the scope is not found, an error is returned instead.

```typescript
{
  type: "command",
  command: "add_scope",
  scope: ScopePath
}
```


### Command: `remove_items`
Removes the specified items from the view.
Responds with an `ack` response.
Does not error if some of the IDs do not exist

```typescript
{
  type: "command",
  command: "remove_items",
  ids: [DisplayedItemRef]
}
```

### Command: `focus_item`

Sets the signal with the specified `DisplayedItemRef` as the _focused_ item.
Responds with an `ack` response if successful, or an error if the item reference
does not exist

```typescript
{
  type: "command",
  command: "focus_item",
  id: DisplayedItemRef
}
```

### Command: `set_viewport_to`

Moves the viewport to center the view on the specified timestamp. Responds with `ack`

```typescript
{
  type: "command",
  command: "set_viewport_to",
  timestamp: number
}
```

### Command: 

Zooms the viewport to fit the whole time range of the currently loaded waveform. Responds with `ack`

```typescript
{
  type: "command",
  command: "zoom_to_fit",
}
```

### Command: `load`

Loads a waveform from the specified file. Responds instantly with
`WcpResponse::ack` if the file is found. If not, an error is returned. Once the
file is loaded, a `waveform_loaded` event is sent to all clients

```typescript
{
  type: "command",
  command: "load",
  source: "string"
}
```

### Command: `reload`

Reloads the waveform from disk if this is possible for the current waveform format.
If it is not possible, this has no effect.
Responds instantly with `ack` response
Once the file is loaded, a `waveform_loaded` event is sent to all clients

```typescript
{
  type: "command",
  command: "reload",
}
```


### Command: `clear`

Removes all currently displayed items. Responds with `ack`
  
```typescript
{
  type: "command",
  command: "clear",
}
```

### Command: `shutdown`

## Message type: Response

All commands have an associated response with the following format that is sent from
the server to the client once it has processed a request
```
S>C:

{
    "type": "response",
    "command": "<command name>",
    "<arg1 name>": "<arg1 value>"
    "<arg2 name>": "<arg2 value>"
    ...
    "<argN name>": "<argN value>"
}
```

### Response: `get_item_list`

```typescript
{
  type: "response",
  command: "get_item_list",
  ids: [DisplayedItemRef]
}
```

### Response: `get_item_info`

```typescript
{
  type: "response",
  command: "get_item_info",
  results: [ItemInfo]
}
```

### Response: `add_variables`

```typescript
{
  type: "response",
  command: "add_variables",
  ids: [DisplayedItemRef]
}
```

### Response: `add_scope`

```typescript
{
  type: "response",
  command: "add_scope",
  ids: [DisplayedItemRef]
}
```

### Response: `ack`

Catch all response for commands that do not return any results

```typescript
{
  type: "response",
  command: "ack",
}
```

## Events

Events can be sent at any time from the server to all clients to notify them of changes
that have happened asynchronously.

The general format is
```
S>C:

{
    "type": "event",
    "event": "<event name>",
    "<arg1 name>": "<arg1 value>"
    "<arg2 name>": "<arg2 value>"
    ...
    "<argN name>": "<argN value>"
}
```

### Event: `waveforms_loaded`

Emitted when a waveform data has been fully loaded 

```typescript
{
  type: "event",
  event: "waveforms_loaded",
}
```
