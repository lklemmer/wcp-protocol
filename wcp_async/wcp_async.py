import asyncio
import logging
import json


class WaveformViewerControlProtocol(asyncio.Protocol):
    def __init__(self):
        loop = asyncio.get_running_loop()
        self.loop = loop
        self.on_con_lost = loop.create_future()
        self.on_greeting = loop.create_future()
        self.buffer = b''
        self.log = logging.getLogger(__name__)
        self.transport = None
        self.commands = None

    def connection_made(self, transport):
        self.transport = transport
        self.log.info("Connected")

    def data_received(self, data):
        self.buffer += data
        while (index := self.buffer.find(b'\x00')) != -1:
            msg = self.buffer[0:index]
            self.buffer = self.buffer[index + 1:]
            self.log.info(f'S>C: {msg!r}')
            json_msg = json.loads(msg)
            # TODO break up responses and notifications
            match json_msg["type"]:
                case "greeting":
                    self.greeting(json_msg)
                case "response":
                    self.on_resp.set_result(json_msg)
                case "notification":
                    self.notify(json_msg)

    def connection_lost(self, exc):
        self.log.info('The server closed the connection')
        self.on_con_lost.set_result(True)

    def send(self, json_msg):
        msg = json.dumps(json_msg).encode()
        self.log.info(f'C>S: {msg!r}')
        msg += b'\x00'
        self.transport.write(msg)

    async def send_command(self, command, args=None):
        if command not in self.commands:
            raise RuntimeError(f"WCP server does not support {command}")

        json_msg = {"type": "command", "command": command}
        if args is not None:
            json_msg.update(args)
        self.on_resp = self.loop.create_future()
        self.send(json_msg)
        await self.on_resp
        resp = self.on_resp.result()
        assert resp["command"] == command
        return resp

    def greeting(self, msg):
        self.commands = msg["commands"]
        self.on_greeting.set_result(True)

    def notify(self, msg):
        self.log.info(f"TODO: handle notifications -- {msg}")

    async def add_variables(self, variables):
        resp = await self.send_command('add_variables', {'names': variables})
        return resp["arguments"]

    async def get_item_list(self):
        resp = await self.send_command('get_item_list')
        return resp["arguments"]

    async def get_item_info(self, ids):
        resp = await self.send_command('get_item_info', {'ids': ids})
        # TODO -- it's weird that this returns the entire response but others return just part of it -- consider encapsulating all response args
        return resp

    async def set_item_color(self, item_id, color):
        await self.send_command('set_item_color', {'id': item_id, 'color': color})

    async def lost_connection(self):
        await self.on_con_lost

    async def greeted(self):
        await self.on_greeting
