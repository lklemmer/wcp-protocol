from wcp.wcp import WcpConnection

with WcpConnection(verbose=False) as wcp:
    wcp.add_variables(['tb.clk', 'tb.reset'])
    item_ids = wcp.get_item_list()
    item_info = wcp.get_item_info(item_ids)
    wcp.set_item_color(item_ids[-1], 'Red')
    wcp.set_item_color(item_ids[-2], 'Yellow')
    wcp.add_scope('tb.dut')
    item_ids = wcp.get_item_list()
    wcp.remove_items([item_ids[-2]])
    del item_ids[-2]
    wcp.focus_item(item_ids[-3])
    wcp.set_viewport_to(600)
