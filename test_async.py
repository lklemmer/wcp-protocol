import asyncio
import logging
from wcp_async.wcp_async import WaveformViewerControlProtocol

async def main():
    loop = asyncio.get_running_loop()

    transport, protocol = await loop.create_connection(
        lambda: WaveformViewerControlProtocol(),
        '127.0.0.1', 54321)

    await protocol.greeted()
    await protocol.add_variables(["tb.clk",
                                  "tb.reset"])
    item_ids = await protocol.get_item_list()
    item_info = await protocol.get_item_info(item_ids)
    await protocol.set_item_color(item_ids[-1], 'Red')
    await protocol.set_item_color(item_ids[-2], 'Yellow')

    transport.close()


console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)

logging.getLogger().addHandler(console_handler)
logging.getLogger().setLevel(logging.INFO)

asyncio.run(main())
